﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace SiMay.Sockets.UtilityHelper
{
    public class LogHelper
    {
        private readonly static Queue<string> _que;
        private readonly static AutoResetEvent _mre;
        private static bool _isRuning = true;
        private static readonly object _lock = new object();
        public static void WriteLog(string log)
        {
            if (!_isRuning)
                return;
            Console.WriteLine(log);
            lock (_lock)
            {
                _que.Enqueue(log);
                _mre.Set();
            }
        }

        static LogHelper()
        {
            _que = new Queue<string>();
            _mre = new AutoResetEvent(false);

            new Thread(new ThreadStart(() =>
            {
                while (_isRuning)
                {
                    _mre.WaitOne();
                    try
                    {
                        var sw = new System.IO.StreamWriter("simay.socket_run.log", true);
                        while (_que.Count > 0)
                        {
                            sw.WriteLine("time：{0} -- {1}", DateTime.Now, _que.Dequeue());
                        }
                        sw.Close();
                    }
                    catch { }
                    Thread.Sleep(1);
                }

                _mre.Close();
            }))
            { IsBackground = true }.Start();
        }

        public static void DisposeLogThread()
        {
            lock (_lock)
            {
                _isRuning = false;
                _mre.Set();
            }
        }
    }
}
